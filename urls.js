module.exports = {
  local: 'http://localhost:3000/',
  prod: 'https://try-dply-prod.herokuapp.com/',
  qa: 'https://try-dply-qa.herokuapp.com/',
}
