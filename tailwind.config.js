module.exports = {
  content: ["./src/**/*.{js,jsx}", "./components/**/*.{html,js,jsx}"],
  important: true,
  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",
        primary: "#38AA90",
        gradPrimary: "#59CEBB",
        secondary: "#9B4ABA",
        gradSecondary: "#AA92EA",
        dark1: "#1B1B1B",
        dark2: "#383838",
        light1: "#9C9C9C",
        light2: "#FFFFFF",
        light3: "#DCFFEE",
      },
      fontFamily: {
        pop: ["Poppins", "sans-serif"],
        pon: ["Pontano Sans", "sans-serif"],
      },
      fontWeight: {
        reg: 400,
        med: 500,
        semi: 600,
      },
      screens: {
        mobile: { raw: "(min-width: 360px)" },
      },
    },
  },
  plugins: [],
};
