import KbhCard from './KbhCard'

const Section2 = () => {
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col justify-center items-center xl:h-[500px] p-4 xl:px-[4rem] py-0">
          <div data-aos="fade" data-aos-duration="1000" className="flex flex-col justify-center items-center pb-6">
            <h1 className="text-center text-[28px] md:text-[36px] xl:text-[42px] tracking-tight font-semi text-dark1 leading-[115%] mb-4">
              Para <span className="text-secondary inline">KawanButuh</span>
            </h1>
            <p className="text-[14px] xl:text-[20px] font-reg text-dark2 text-justify md:text-center tracking-tight mb-0">
              KawanButuh adalah orang yang membutuhkan bantuan dari para KawanBantu <span className="inline md:block">yuk kita bantu para KawanButuh</span>
            </p>
          </div>
          <div>
            <KbhCard />
          </div>
          <div data-aos="fade-up" data-aos-duration="1000" className="flex pt-10">
            <div className="hover:opacity-70">
              <a
                href="https://app.kawanbantu.com/nearme"
                className="text-[14px] md:text-[16px] hover:text-[16px] outline outline-1 text-primary font-reg py-2 px-3 rounded-md"
              >
                Lihat Semua
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Section2
