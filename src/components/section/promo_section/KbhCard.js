import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { staticData, kbhId } from './staticData'
import { toRupiah } from '../../../utils/lib'
import iconMarker from '../../../assets/svg/KbhCard/iconMarker.svg'
import iconHand from '../../../assets/svg/KbhCard/iconHand.svg'

const KbhCard = () => {
  const guessToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJQMmMzQ1U0LVNyQjhOMWR5bXFsQ0QiLCJndWVzdCI6dHJ1ZSwia2JpIjpmYWxzZSwiaWF0IjoxNjUxMjM2MjM0LCJleHAiOjE2ODAyNjY2MzR9.ELZlZojuFi4g5ZWWQiJjB2XmR9fxbLVDIwtsm6fzrA8'
  const baseURL = 'https://main.apis.dev.kawanbantu.com'
  const [resData, setResData] = useState([])

  const requests = []
  for (let i = 0; i < kbhId.length; i++) {
    requests.push(`${baseURL}/kawanbutuh/${kbhId[i]}`)
  }

  const getAllData = async () => {
    const responses = await axios.all(
      requests.map((request) =>
        axios.get(request, {
          headers: { Authorization: `Bearer ${guessToken}` },
        })
      )
    )
    const promises = await Promise.all(responses)
    const response = promises.map((res) => res.data)
    for (let i = 0; i < response.length; i++) {
      response[i] = { ...staticData[i], ...response[i] }
    }
    setResData(response)
  }

  useEffect(() => {
    getAllData()
    return () => {
      setResData([])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <div data-aos="fade-up" data-aos-duration="1000" className="grid xl:grid-cols-4 md:grid-cols-2 grid-cols-1 gap-10">
        {resData?.map((item) => (
          <a key={item.id} href={item.profile_link}>
            <div className="flex justify-center hover:opacity-80">
              <div className="rounded-lg shadow-lg bg-white max-w-sm">
                <img className="rounded-t-lg" src={item.card_image} alt="" />
                <div className="py-2 px-3">
                  <h4 className="text-light1 text-xs mb-1">KawanButuh</h4>
                  <h4 className="text-dark2 text-sm font-semi mb-1">{item.name}</h4>
                  <p className="text-dark2 text-xs mb-3">
                    <img className="inline pr-1" src={iconMarker} alt="" />
                    {item.kbh_location}
                  </p>
                  <div className="flex">
                    <p className="text-dark2 text-xs font-semi mb-2">{toRupiah(item.donation_amount)}</p>
                    <p className=" text-xs font-reg mx-1 mr-auto">terkumpul.</p>
                    <p className="text-primary text-xs font-semi mb-2">
                      <img className="inline pr-1" src={iconHand} alt="" />
                      {item.donation_count} Orang
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </a>
        ))}
      </div>
    </>
  )
}

export default KbhCard
