import React from 'react';

const Section2 = () => {
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col justify-center items-center md:h-[400px] p-4 xl:p-[4rem]">
          <div
            data-aos="fade"
            data-aos-duration="1000"
            className="flex flex-col justify-center items-start md:items-center p-0 xl:px-10 h-fit"
          >
            <div className="mx-auto font-pon text-[80px] md:text-[122px] xl:text-[144px] mb-[-60px] md:mb-[-100px] xl:mb-[-110px] text-secondary">
              “”
            </div>
            <h1 className="text-center text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] mb-4">
              Internet please do your magic
            </h1>
            <p className="text-[14px] xl:text-[20px] font-reg text-light1 text-justify md:text-center tracking-tight mb-2">
              Sering membaca kalimat ini di media sosial?
            </p>
            <div>
              <p className="inline text-[14px] xl:text-[20px] font-reg text-light1 text-justify md:text-center tracking-tight">
                Walaupun banyak yang terbantu, dari{' '}
              </p>
              <p className="inline text-[16px] xl:text-[22px] font-semi text-secondary text-justify md:text-center tracking-tight">
                27 juta{' '}
              </p>
              <p className="inline text-[14px] xl:text-[20px] font-reg text-light1 text-justify md:text-center tracking-tight">
                orang miskin di Indonesia,{' '}
              </p>
              <p className="inline md:block text-[14px] xl:text-[20px] font-reg text-light1 text-justify md:text-center tracking-tight">
                {' '}
                belum semuanya mendapatkan kesempatan yang sama.
              </p>
              <p className="text-[14px] xl:text-[20px] font-reg text-primary text-justify md:text-center tracking-tight mt-2">
                Source: Badan Pusat Statistik 2021
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section2;
