import card1 from '../../../assets/images/KbhCard/card1.webp'
import card2 from '../../../assets/images/KbhCard/card2.webp'
import card3 from '../../../assets/images/KbhCard/card3.webp'
import card4 from '../../../assets/images/KbhCard/card4.webp'

export const staticData = [
  {
    card_image: card1,
    profile_link: 'https://app.kawanbantu.com/nearme/details/1511',
    kbh_location: 'Jl. Malaka III, Duren Sawit',
    donation_count: '4',
  },
  {
    card_image: card2,
    profile_link: 'https://app.kawanbantu.com/nearme/details/1458',
    kbh_location: 'Jl. Poksai No.18',
    donation_count: '10',
  },
  {
    card_image: card3,
    profile_link: 'https://app.kawanbantu.com/nearme/details/1426',
    kbh_location: 'Jalan Haji Nawin, Jati Cempaka, Bekasi.',
    donation_count: '6',
  },
  {
    card_image: card4,
    profile_link: 'https://app.kawanbantu.com/nearme/details/1428',
    kbh_location: 'Jembatan Curug, Jakarta Timur.',
    donation_count: '2',
  },
]

export const kbhId = ['1511', '1458', '1426', '1428']
