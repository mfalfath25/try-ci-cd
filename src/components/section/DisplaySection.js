import React from 'react';
import Section1 from './display_section/Section1';
import Section2 from './display_section/Section2';
import Section3 from './display_section/Section3';
import Section4 from './display_section/Section4';

const DisplaySection = () => {
  return (
    <>
      <Section1 />
      <div className="spacer h-[20vh] lg:h-0"></div>
      <Section2 />
      <div className="spacer h-[20vh] lg:h-0"></div>
      <Section3 />
      <div className="spacer h-[20vh] lg:h-0"></div>
      <Section4 />
    </>
  );
};

export default DisplaySection;
