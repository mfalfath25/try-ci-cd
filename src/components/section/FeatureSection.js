import React from 'react';

const FeatureSection = () => {
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col justify-center items-center md:h-[900px] p-4 xl:p-[4rem]">
          <div className="absolute container rounded-lg sm:left-auto h-[700px] lg:h-[1000px] bg-green-100 md:bg-gradient-to-r from-green-100 to-purple-100 blur-3xl z-[-1]" />
          <div
            data-aos="fade"
            data-aos-duration="1000"
            className="flex p-6 lg:p-4 xl:p-6 2xl:p-10 justify-center items-end basis-0 sm:basis-1/4 "
          >
            <h1 className="text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] text-center">
              Fitur unggulan di
              <span className="text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] inline md:block">
                {' '}
                Aplikasi{' '}
                <span className="text-secondary inline">KawanBantu</span>
              </span>
            </h1>
          </div>
          <div
            data-aos="fade-up"
            data-aos-duration="1000"
            className="grid xl:grid-cols-4 sm:grid-cols-2 grid-cols-1 gap-10 pt-5"
          >
            {[
              [
                '1',
                'Lebih Transparan',
                'Bantuan langsung disalurkan oleh KawanBantu, tanpa pihak ketiga',
                'Feature1',
              ],
              [
                '2',
                'Berkelanjutan',
                'Donasi kepada orang yang sama secara berulang kali kapanpun',
                'Feature2',
              ],
              [
                '3',
                'Berbasis Lokasi',
                'Temukan orang yang membutuhkan di sekitarmu',
                'Feature3',
              ],
              [
                '4',
                'Bantu Upload',
                'Upload data mereka yang butuh dengan mudah ke KawanBantu',
                'Feature4',
              ],
            ].map(([id, title, desc, image]) => (
              <div className="flex justify-center" key={id}>
                <div>
                  <img
                    src={require(`../../assets/svg/Feature/${image}.svg`)}
                    alt="..."
                    className="max-w-[50%] sm:max-w-[60%] xl:max-w-[70%] h-auto mx-auto"
                  />
                  <div className="font-semi text-[24px] md:text-[26px] lg:text-[28px] xl:text-[30px] text-dark1 leading-[115%] my-2 md:my-4 md:mb-4 text-center">
                    {title}
                  </div>
                  <div className="font-reg text-[14px] lg:text-[18px] text-light1 w-[240px] lg:w-[300px] tracking-tight px-0 lg:px-[0] md:px-[8px] text-center mx-auto">
                    {desc}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default FeatureSection;
