import React from 'react';
import MockupHero from '../../assets/images/MockupDevice.webp';

const HeroSection = () => {
  return (
    <>
      <div className="md:from-purple-100 md:bg-gradient-to-t">
        <div
          data-aos="fade"
          data-aos-duration="1000"
          className="container mx-auto"
        >
          <div className="grid grid-cols-1 lg:grid-cols-2 gap-10 justify-between items-center md:h-[850px] p-4 xl:p-[4rem]">
            <div className="flex flex-col justify-center order-last lg:order-first">
              <h1 className="text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] mb-4">
                Platform donasi yang memberikan dampak berkelanjutan kepada yang
                butuh
              </h1>
              <p className="text-[14px] xl:text-[20px] font-reg text-light1 text-justify tracking-tight mb-6">
                Kampanye kami 24 jam dan berkelanjutan. Bantu mereka terus
                menerus sehendakmu. Buka aplikasi KawanBantu sekarang!
              </p>
              <div className="flex flex-row justify-start xl:justify-start">
                <a
                  type="submit"
                  href="https://app.kawanbantu.com"
                  className="text-[14px] md:text-[20px] bg-primary hover:shadow-2xl hover:opacity-80 text-light2 font-reg py-3 md:py-4 px-4 md:px-6 rounded-md"
                >
                  Buka App
                </a>
              </div>
            </div>
            <div className="flex flex-col justify-center items-center px-4 sm:px-0">
              <img
                src={MockupHero}
                alt="..."
                className="max-w-full h-auto sm:max-w-md lg:max-w-full"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeroSection;
