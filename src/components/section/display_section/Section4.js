import React from 'react';
import Display4 from '../../../assets/images/Display/Display4.webp';

const Section4 = () => {
  return (
    <>
      <div
        data-aos="fade"
        data-aos-duration="1000"
        className="container mx-auto"
      >
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-2 lg:gap-10 justify-between items-center md:h-[850px] p-4 xl:p-[4rem]">
          <div className="flex flex-col justify-center order-last lg:order-first">
            <h3 className="text-[18px] md:text-[20px] lg:text-[24px] tracking-tight font-med text-primary">
              #BantuUpload
            </h3>
            <h2 className="text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] py-4">
              Bantu Mereka yang{' '}
              <span className="text-secondary">Belum Terjangkau</span>
            </h2>
            <p className="text-[14px] xl:text-[20px] font-reg text-dark2 text-justify tracking-tight mb-6">
              Upload data mereka yang butuh ke KawanBantu agar bisa ditemukan
              donatur-donatur lain. Cukup upload saja kami yang urus verifikasi,
              penyaluran dan pelaporannya.
            </p>
          </div>
          <div className="flex flex-col justify-center items-center px-4 sm:px-0">
            <img
              src={Display4}
              alt="..."
              className="max-w-full h-auto sm:max-w-md lg:max-w-full mb-5 sm:mb-0"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Section4;
