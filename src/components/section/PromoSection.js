import React from 'react';
import Section1 from './promo_section/Section1';
import Section2 from './promo_section/Section2';

const PromoSection = () => {
  return (
    <>
      <Section1 />
      <div className="spacer h-[20vh] lg:h-0"></div>
      <Section2 />
    </>
  );
};

export default PromoSection;
