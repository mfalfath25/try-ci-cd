import React from 'react';
import MockupFooter from '../../assets/images/MockupFooter.webp';

const DemoSection = () => {
  return (
    <>
      <div className="md:bg-green-50">
        <div className="container mx-auto">
          <div className="flex flex-col lg:flex-row justify-between items-center h-fit p-4 pb-0 xl:p-[4rem] xl:pb-0">
            <div className="relative flex basis-1/2">
              <div className="flex flex-col lg:pr-20 xl:pr-20 2xl:pr-30">
                <h1 className="text-[28px] md:text-[36px] xl:text-[48px] tracking-tight font-semi text-dark1 leading-[115%] mb-4 text-left">
                  Ayo kawan
                  <span className="text-secondary block">Bantu mereka!</span>
                </h1>
                <p className="text-[14px] xl:text-[20px] font-reg text-dark1 text-justify tracking-tight">
                  Bantuan aman dan transparan ke orang yang sama untuk menjamin
                  dampak yang berkelanjutan. Buka aplikasinya sekarang!
                </p>
                <div className="flex flex-row mt-6 justify-start lg:justify-center xl:justify-start">
                  <a
                    type="submit"
                    href="https://app.kawanbantu.com"
                    className="text-[14px] md:text-[20px] bg-primary hover:shadow-2xl hover:opacity-80 text-light2 font-reg py-3 md:py-4 px-4 md:px-6 rounded-md"
                  >
                    Buka App
                  </a>
                </div>
              </div>
            </div>
            <div className="flex basis-1/2 w-full justify-center items-center xl:items-end pt-[2rem] px-[2rem] lg:p-0">
              <img
                src={MockupFooter}
                alt="..."
                className="max-w-[60%] lg:max-w-[60%]"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DemoSection;
