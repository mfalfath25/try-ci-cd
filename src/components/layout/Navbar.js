import React from 'react';
import Logo from '../../assets/images/Logo.png';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <div className="container mx-auto block p-4 xl:px-[3rem] xl:py-0">
      <div className="flex flex-row">
        <Link to="/">
          <img src={Logo} alt="..." />
        </Link>
        <nav className="p-0 m-0 w-full relative flex items-center justify-end">
          {[['Login', 'https://app.kawanbantu.com/login']].map(
            ([title, url]) => (
              <a
                key={title}
                href={url}
                className="text-[14px] md:text-[16px] hover:text-[16px] outline outline-1 hover:opacity-70 text-light1 font-reg py-2 px-3 rounded-md"
              >
                {title}
              </a>
            )
          )}
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
