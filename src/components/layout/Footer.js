import React from 'react';
import Logo from '../../assets/images/Logo.png';
import Payment from '../../assets/svg/Footer/Payment.svg';

const Footer = () => {
  return (
    <div className="bg-dark1 sticky top-[100%]">
      <div className="container mx-auto block">
        <div className="flex flex-col md:flex-row justify-between p-4 xl:pb-[2rem] xl:p-[3rem]">
          <div className="flex justify-start items-start">
            <img src={Logo} alt="..." className="px-4 md:px-0" />
          </div>
          <div className="p-4">
            <div className="font-semi text-[18px] pb-[10px] text-light2">
              Contact
            </div>
            <div className="font-reg text-[14px] lg:text-[16px] py-[4px] text-light2">
              +628231264835
            </div>
            <div className="font-reg text-[14px] lg:text-[16px] py-[4px] text-light2">
              kawanbantu@gmail.com
            </div>
            <div className="font-reg text-[14px] lg:text-[16px] py-[4px] text-light2">
              Instagram.com/kawanbantu
            </div>
          </div>
          <div className="p-4">
            <div className="font-semi text-[18px] pb-[10px] text-light2">
              Address
            </div>
            <div className="font-reg text-[14px] lg:text-[16px] py-[4px] max-w-[250px] text-light2">
              QP office, tanjung mas raya blok B1 no.44, Jagakarsa, Jakarta
              Selatan 12530
            </div>
          </div>
          <div className="p-4">
            <div className="font-semi text-[18px] pb-[10px] text-light2">
              Metode Pembayaran
            </div>
            <img src={Payment} alt="..." />
          </div>
        </div>
      </div>

      <div className="bg-dark1 flex flex-col md:flex-row h-fit">
        <div className=" text-light2 flex mx-auto text-[14px] lg:text-[16px] mb-4">
          © Copyright 2022 KawanBantu. All rights reserved
        </div>
      </div>
    </div>
  );
};

export default Footer;
