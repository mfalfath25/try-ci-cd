import React, { useState, useEffect } from 'react'
import './index.css'
import { Routes, Route, Navigate } from 'react-router-dom'
import Landing from './pages/Landing'
import Logo from './assets/images/Logo.png'

function App() {
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    }, 500)
  }, [])

  return (
    <div className="overflow-x-hidden">
      {loading ? (
        <div className="container flex mx-auto justify-center items-center h-screen">
          <img id="kb-logo" src={Logo} alt="..." />
        </div>
      ) : (
        <Routes>
          <Route path="*" element={<Navigate to={'/'} />} />
          <Route path="/" element={<Landing />} />
        </Routes>
      )}
    </div>
  )
}

export default App
