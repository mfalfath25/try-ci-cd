import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Navbar from '../components/layout/Navbar';
import HeroSection from '../components/section/HeroSection';
import PromoSection from '../components/section/PromoSection';
import FeatureSection from '../components/section/FeatureSection';
import DisplaySection from '../components/section/DisplaySection';
import DemoSection from '../components/section/DemoSection';
import Footer from '../components/layout/Footer';

const Landing = () => {
  AOS.init({
    once: true,
  });

  return (
    <>
      <div className="min-h-screen mx-auto">
        <Navbar />
        <HeroSection />
        <div className="spacer h-[20vh] lg:h-0"></div>
        <PromoSection />
        <div className="spacer h-[20vh] xl:h-0"></div>
        <FeatureSection />
        <div className="spacer h-[20vh] lg:h-0"></div>
        <DisplaySection />
        <div className="spacer h-[20vh] lg:h-0"></div>
        <DemoSection />
        <Footer />
      </div>
    </>
  );
};

export default Landing;
